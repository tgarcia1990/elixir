defmodule Calculadora do
  @moduledoc """
  Documentation for `Calculadora`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Calculadora.hello()
      :world

  """
  def hello do
    :world
  end
######################## tarea 1
  def add(num1,num2) do
    "#{num1} + #{num2} = #{num1+num2}"
  end

  def sub(num1,num2) do
    "#{num1} - #{num2} = #{num1-num2}"
  end

  def mult(num1,num2) do
    "#{num1} * #{num2} = #{num1*num2}"
  end

  def pow(num1) do
    "pow #{num1} = #{:math.pow(num1,2)}"
  end

  def div2(num1,num2) do
    cond do
      num2 == 0 ->
      " bad argument in arithmetic expression"
      true ->
      "#{num1} / #{num2} = #{num1/num2} rem= #{rem(num1,num2)}"
      #"#{num1} / #{num2} = #{num1/num2}, div= #{div(num1,num2)}, rem= #{rem(num1,num2)}"
    end
  end

############## tarea 2

############ ejercicio 1 tarea 2
  @doc """
  flatten.

  ## Examples

      iex>  Calculadora.flatten([1, [2, [3, [4, [5]]]]])
      [1, 2, 3, 4, 5]

  """

  #def flatten(list, tail) do
  # :lists.flatten(list, tail)
  #end
##con recursividad
  def flatten([head | tail]), do: flatten(head) ++ flatten(tail)
  def flatten([]), do: []
  def flatten(element), do: [element]


############ ejercicio 2 tarea 2
  @doc """
  palindrome.

  ## Examples

      iex>  Calculadora.palindrome("a luna ese anula")
      " list is palindrome"

  """

  def palindrome(list) do

  value1 = String.split(list, "", trim: true)
  |>Enum.reverse
  |>Enum.join

  cond do
      String.replace(list, " ", "")==String.replace(value1, " ", "") ->
      " #{list} is palindrome"
      true ->
      "#{list} is not palindrome"
    end

  end
########### ejercicio clase, suma invertida
  def addrev(n) when n == 0 do
    0
  end

  def addrev(n) do
    n+addrev(n-1)
  end

############# ejemplo de recursividad
  def print_multiple_times(msg, n) when n <= 1 do
    IO.puts "#{msg} 1"
  end

  def print_multiple_times(msg, n) do
    IO.puts msg
    print_multiple_times(msg, n - 1)

  end
############################ tarea 3

 @doc """
  keywordkl.

  ## Examples

      iex> Calculadora.keywordkl

  """

#ejercicio 1

 def keywordkl do
    kl = [name: "Ada Lovelace", born: 1815, died: 1852, known_for: "mathematics", known_for: "computing" ]
    IO.puts "#{Keyword.get(kl, :name)} was known for #{Enum.join(Keyword.get_values(kl,:known_for), " and ")}"
    #IO.puts "#{Keyword.get(kl, :name)} was known for #{Keyword.get_values(kl,:known_for)|>Enum.join(" and ")}"
  end

 @doc """
  maptest.

  ## Examples

      iex> Calculadora.maptest

  """

#ejercicio 2
  def maptest do
  #Joe Armstrong: Programming Erlang — Software for a Concurrent World (2nd edition), The Pragmatic Bookshelf, 2013.
    map = %{:author => "Joe Armstrong", :name => "Programming Erlang — Software for a Concurrent World (2nd edition)",
    :title => "The Pragmatic Bookshelf",:year=>2013}
    IO.puts "#{map[:author]}: #{map[1]}, #{map[:title]}, #{map[3]}"
  end

 @doc """
  listmap.

  ## Examples

      iex> Calculadora.maptest

  """

#ejercicio 3
  def listmap do
  #Joe Armstrong: Programming Erlang — Software for a Concurrent World (2nd edition), The Pragmatic Bookshelf, 2013.
  #F. Cesarini and S. Thompson: Erlang Programming — A Concurrent Approach to Software Development, O’Reilly, 2009.
  #M. Logan, E. Merrit, R. Carlsson: Erlang and OTP in Action, Manning Eds, 2010.

  list_with_maps =[
    %{:author => "Joe Armstrong", :name => "Programming Erlang — Software for a Concurrent World (2nd edition)",
    :title => "The Pragmatic Bookshelf",:year=>2013},
    %{:author => "Cesarini and S. Thompson", :name => "Thompson: Erlang Programming — A Concurrent Approach to Software Development",
    :title => "O’Reilly",:year=>2009},
    %{:author => "M. Logan", :name => "E. Merrit, R. Carlsson: Erlang and OTP in Action",
    :title => "Manning Eds",:year=>2010}
    ]
    #list_with_maps =[
    #  id1: %{:author => "Joe Armstrong", :name => "Programming Erlang — Software for a Concurrent World (2nd edition)",
    #  :title => "The Pragmatic Bookshelf",:year=>2013},
    #  id2: %{:author => "Cesarini and S. Thompson", :name => "Thompson: Erlang Programming — A Concurrent Approach to Software Development",
    #  :title => "O’Reilly",:year=>2009},
    #  id3: %{:author => "M. Logan", :name => "E. Merrit, R. Carlsson: Erlang and OTP in Action",
    #  :title => "Manning Eds",:year=>2010}
    #  ]
    #IO.puts "#{map[:author]}: #{map[1]}, #{map[:title]}, #{map[3]}"
    #list_with_maps.flatten(Enum.map(a, fn(x) -> Map.values(x) end))
    #Enum.map(list_with_maps, &(Map.take(&1, ["author"])))
    #contacts = [%{"1" => ["0724573977"]}, %{"2" => ["0724573111", "0744556778"]}]
    #[%{"1" => ["0724573977"]}, %{"2" => ["0724573111", "0744556778"]}]
    #contacts
    #|> Enum.map(&Map.values/1)
    #|> List.flatten
    #list_with_maps = [%{:id => 1, :name => "a"}, %{:id => 2, :name => "b"}]
    #Enum.map(list_with_maps, fn (x) -> x[:id] end)

    #ejemplos obtner data
    Enum.map(list_with_maps, fn (x) -> x[:id] end)
    #Enum.map(list_with_maps, fn (map) -> Map.take(map, [:id,:name]) end)
  end

 @doc """
  listmapejer4.

  ## Examples

      iex> Calculadora.get_data

  """

  #ejercicio 4
  def listmapejer4 do

     [
      %{:author => "Joe Armstrong", :name => "Programming Erlang — Software for a Concurrent World (2nd edition)",
      :title => "The Pragmatic Bookshelf",:year=>2013},
      %{:author => "Cesarini and S. Thompson", :name => "Thompson: Erlang Programming — A Concurrent Approach to Software Development",
      :title => "O’Reilly",:year=>2009},
      %{:author => "M. Logan", :name => "E. Merrit, R. Carlsson: Erlang and OTP in Action",
      :title => "Manning Eds",:year=>2010}
      ]

    end

    def get_data(coll \\ listmapejer4()) do
     coll
      |> Enum.group_by(&(&1.year<2011))
      |>Map.delete(:false)


      #coll[:2010].name
      #|>Enum.map( fn (map) -> Map.take(map, [:name]) end)
      |> Enum.map(fn {_letter_grade, subset} -> Enum.sort(subset) end)

    end

###ejemplos para ejercicio 3 y 4

  def listmap3 do

     # list = [%{"id": 1, "role": ["A", "B"]}, %{"id": 2, "role": ["B", "C"]}, %{"id": 1, "role": ["C", "A"]} ]

      #list
      #|> Enum.group_by(&(&1.id))
      #|> Enum.map(fn {key, value} ->
      #  %{id: key, role: value |> Enum.flat_map(&(&1.role)) |> Enum.uniq}
      #end)
      #
  end

  def list_exam_results do
    [
      %{name: "Zelda", score: 56},
      %{name: "Eddy", score: 58},
      %{name: "Jack", score: 69},
      %{name: "Abby", score: 84},
      %{name: "Lily", score: 96}
    ]
  end
  def by_grade(%{score: score}) do
    cond do
      score < 65 -> "F"
      score < 70 -> "D"
      score < 80 -> "C"
      score < 90 -> "B"
      true -> "A"
    end
  end

  def prepare_records(coll \\ list_exam_results()) do
    coll
    |> Enum.group_by(&by_grade(&1))
    |> Enum.map(fn {_letter_grade, subset} -> Enum.sort(subset) end)
    |> IO.inspect
  end
#### ejercicio dia 4
def main(input) do
    input
    |> hash_input
    |> pick_color
    |> build_grid
    |> filter_odd_squares
    |> build_pixel_map
    |> draw_image
    |> save_image(input)
  end

  def save_image(image, input) do
    File.write("#{input}.png", image)
  end

  def draw_image(%Calculadora.Image{color: color, pixel_map: pixel_map}) do
    image = :egd.create(250, 250)
    fill = :egd.color(color)

    Enum.each pixel_map, fn({start, stop}) ->
      :egd.filledRectangle(image, start, stop, fill)
    end

    :egd.render(image)
  end

  def build_pixel_map(%Calculadora.Image{grid: grid} = image) do
    pixel_map = Enum.map grid, fn({_code, index}) ->
      horizontal = rem(index, 5) * 50
      vertical = div(index, 5) * 50

      top_left = {horizontal, vertical}
      bottom_right = {horizontal + 50, vertical + 50}

      {top_left, bottom_right}
    end

    %Calculadora.Image{image | pixel_map: pixel_map}
  end

  def filter_odd_squares(%Calculadora.Image{grid: grid} = image) do
    grid = Enum.filter grid, fn({code, _index}) ->
      rem(code, 2) == 0
    end

    %Calculadora.Image{image | grid: grid}
  end

  def build_grid(%Calculadora.Image{hex: hex} = image) do
    grid =
      hex
      |> Enum.chunk(3)
      |> Enum.map(&mirror_row/1)
      |> List.flatten
      |> Enum.with_index

    %Calculadora.Image{image | grid: grid}
  end

  def mirror_row(row) do
    [first, second | _tail] = row

    row ++ [second, first]
  end

  def pick_color(%Calculadora.Image{hex: [r, g, b | _tail]} = image) do
    %Calculadora.Image{image | color: {r, g, b}}
  end

  def hash_input(input) do
    hex = :crypto.hash(:md5, input)
    |> :binary.bin_to_list

    %Calculadora.Image{hex: hex}
  end




end
